
# Create your views here.
# from rest_framework import viewsets
# from transactions_ai_service.models.transaction import Transaction
# from transactions_ai_service.serializers.transactionSerializer import TransactionSerializer

# class TransactionViewSet(viewsets.ModelViewSet):
#    """
#    API endpoint that allows transactions to be viewed or edited.
#    """
#    queryset = Transaction.objects.all().order_by('-date')
#    serializer_class = TransactionSerializer

from transactions_ai_service.models.transaction import Transaction
from django.shortcuts import get_object_or_404
from transactions_ai_service.serializers.transactionSerializer import TransactionSerializer
from rest_framework import viewsets
from rest_framework.response import Response

class TransactionViewSet(viewsets.ViewSet):

    """
    A default ViewSet for listing or retrieving users, for any unrepresented methods
    """
    queryset = Transaction.objects.all().order_by('-date')
    serializer_class = TransactionSerializer

    def list(self, request):
        queryset = Transaction.objects.all().order_by('-date')
        serializer = TransactionSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        # 0 - Do a tensorflow thing then return another thing (for test)
        # 1 - Do a simple TensorFlow action (needs research)
        # 2 - Return the JSON shape we want (exists now, +id, +predictedCategory)
        # 3 - Add AI Element
        pass

    def retrieve(self, request, pk=None):
        queryset = Transaction.objects.all().order_by('-date')
        user = get_object_or_404(queryset, pk=pk)
        serializer = TransactionSerializer(user)
        return Response(serializer.data)

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass