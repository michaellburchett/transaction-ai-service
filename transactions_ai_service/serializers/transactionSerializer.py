from rest_framework import serializers

from transactions_ai_service.models.transaction import Transaction

class TransactionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Transaction
        fields = ('date', 'description', 'amount', 'category', 'pay_to')