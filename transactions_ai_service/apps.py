from django.apps import AppConfig


class TransactionsAiServiceConfig(AppConfig):
    name = 'transactions_ai_service'