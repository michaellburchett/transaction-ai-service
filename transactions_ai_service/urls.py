from django.urls import include, path
from rest_framework import routers

from transactions_ai_service.views import views, transactionViews

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)

# transaction URLs
router.register(r'transactions', transactionViews.TransactionViewSet, basename='transaction')


urlpatterns = [
    # Wire up our API using automatic URL routing.
    # Additionally, we include login URLs for the browsable API.
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]