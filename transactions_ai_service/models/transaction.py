from django.db import models

# Create your models here.

# RuntimeError: Model class transactions_ai_service.models.transaction.Transaction doesn't declare an
# explicit app_label and isn't in an application in INSTALLED_APPS.

class Transaction(models.Model):
    date = models.DateField()
    description = models.CharField("A description of the charge", max_length=30)
    amount = models.DecimalField("The amount for the transaction", max_digits=5, decimal_places=2)
    category = models.CharField("The categorization for the field", max_length=30)
    pay_to = models.CharField("Who the recipient is for the transaction", max_length=30)